GRANT ALL PRIVILEGES ON *.* TO 'root'@'172.28.0.2' IDENTIFIED BY 'your_password' WITH GRANT OPTION;
FLUSH PRIVILEGES;


CREATE TABLE users (
                       id INT AUTO_INCREMENT PRIMARY KEY,
                       category VARCHAR(255) NOT NULL,
                       firstname VARCHAR(255) NOT NULL,
                       lastname VARCHAR(255) NOT NULL,
                       email VARCHAR(255) NOT NULL,
                       gender ENUM('male', 'female', 'other') NOT NULL,
                       birthDate DATE NOT NULL
);
