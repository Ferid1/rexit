<?php
namespace utils;

class DataExporter
{
    public function exportToCSV($data, $filename = 'export.csv')
    {

        if (headers_sent()) {
            throw new \Exception("Headers already sent. Cannot export CSV.");
        }

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '"');

        $output = fopen('php://output', 'w');
        if ($output === false) {
            throw new \Exception("Failed to open output stream for CSV export.");
        }

        foreach ($data as $row) {
            if (is_array($row)) {
                fputcsv($output, $row);
            } else {
                throw new \Exception("Data row is not an array.");
            }
        }
        fclose($output);
    }
}
?>
