<?php

namespace utils;

class CSVReader
{
    public function readCSV($filePath, $callback)
    {
        if (!file_exists($filePath)) {
            throw new \Exception("File does not exist: {$filePath}");
        }

        if (!is_readable($filePath)) {
            throw new \Exception("File is not readable: {$filePath}");
        }

        $handle = fopen($filePath, "r");
        if ($handle === FALSE) {
            throw new \Exception("Unable to open file: {$filePath}");
        }

        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            // Use a callback function to process each row
            call_user_func($callback, $data);
        }

        fclose($handle);
    }
}
