<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bootstrap Styled Page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-6">

            <form action="index.php" method="post" enctype="multipart/form-data">
                <input type="file" name="csvFile">
                <input type="submit" value="Upload CSV">
            </form>

            <form class="mb-3" action="index.php" method="get">
                Category: <input type="text" name="category"><br>
                Gender: <input type="text" name="gender"><br>
                Birth Date: <input type="date" name="birthDate"><br>
                Age: <input type="number" name="age"><br>
                Age Range: <input type="text" name="ageRange" placeholder="25-30"><br>
                <button type="submit" class="btn btn-primary">Filter</button>
            </form>

            <form class="mb-3" action="index.php" method="get">
                <input type="hidden" name="category" value="<?php echo htmlspecialchars($_GET['category'] ?? ''); ?>">
                <input type="hidden" name="gender" value="<?php echo htmlspecialchars($_GET['gender'] ?? ''); ?>">
                <input type="hidden" name="birthDate" value="<?php echo htmlspecialchars($_GET['birthDate'] ?? ''); ?>">
                <input type="hidden" name="age" value="<?php echo htmlspecialchars($_GET['age'] ?? ''); ?>">
                <input type="hidden" name="ageRange" value="<?php echo htmlspecialchars($_GET['ageRange'] ?? ''); ?>">

                <button type="submit" name="export" class="btn btn-secondary">Export to CSV</button>
            </form>

            <?php if (!empty($data)): ?>

                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Email</th>
                        <th>Gender</th>
                        <th>BirthDate</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($data as $row): ?>
                        <tr>
                            <td><?php echo htmlspecialchars($row['category']); ?></td>
                            <td><?php echo htmlspecialchars($row['firstname']); ?></td>
                            <td><?php echo htmlspecialchars($row['lastname']); ?></td>
                            <td><?php echo htmlspecialchars($row['email']); ?></td>
                            <td><?php echo htmlspecialchars($row['gender']); ?></td>
                            <td><?php echo htmlspecialchars($row['birthDate']); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                <nav>
                    <ul class="pagination">
                        <?php
                        function createPageLink($pageNum, $filters) {
                            $link = '?page=' . $pageNum;
                            foreach ($filters as $key => $value) {
                                if (!empty($value)) {
                                    $link .= "&$key=" . urlencode($value);
                                }
                            }
                            return $link;
                        }
                        $range = 2; // Number of links to show before and after the current page
                        $firstPage = max(1, $page - $range);
                        $lastPage = min($totalPages, $page + $range);

                        if ($firstPage > 1) {
                            echo '<li class="page-item"><a class="page-link" href="' . createPageLink(1, $filters) . '">First</a></li>';
                        }

                        for ($i = $firstPage; $i <= $lastPage; $i++) {
                            echo '<li class="page-item';
                            if ($i === $page) echo ' active';
                            echo '"><a class="page-link" href="' . createPageLink($i, $filters) . '">' . $i . '</a></li>';
                        }

                        if ($lastPage < $totalPages) {
                            echo '<li class="page-item"><a class="page-link" href="' . createPageLink($totalPages, $filters) . '">Last</a></li>';
                        }
                        ?>
                    </ul>
                </nav>


            <?php endif; ?>

        </div>
    </div>
</div>

</body>
</html>
