# Project Name

## Quick Start


1. **Clone the repo**:
   git clone https://gitlab.com/Ferid1/rexit



2. **Run with Docker**:
   docker-compose build &
   docker-compose up


3. **Access the service**:
   Open your web browser and visit `localhost:8000`.

**Done!** Your service is now up and running.
